#!/bin/bash

# Climate script, optimized for a specific country.
# This script finds all WMO stations which report weather observations in SYNOP code in a user selected country downloads the data and decodes the data from the available values for temperature, humidity, dew point temperature, wind speed, wind direction, station pressure, sea level pressure and sea surface temperature. Sea surface temperature (SST) data is taken in synop group 222, because that's how they transmit it from Varna and Burgas. SST data is probably not valid for countries other than Bulgaria!
# If you need data only for a specific country, for example Bulgaria, getsynop allows you to download data for a whole country via the state parameter, which will take less time. This is implemented in the current script. Even if the region you need doesn't cover the whole country, but only a part of it, this script will perform faster. Usually surrounding countries are also included, because that's what the ogimet queries return, but data for these surrouning countries won't be available for the whole period, so it is best to download them separately.
# The script still takes a lot of time, because it waits 3-10 minutes between each query to ogimet. It's recommended to run it in the background with the output written in log files.

##############################################################################################################
# USER SPECIFICATIONS

# A wmo file name with all the wmo codes to insert station metadata in each data file.
# Note: You can decrease the time it takes for decode_synop.x to arrange the data in
# tables if you have only the relevant WMO indices in the wmo-intex-all file.
WMO_FILE=wmo-index-all

# Optimized for a single country, you need the country code. Theoretically, you can change the state string and coordinates to any other country and it will be optimized for there. This is because getsynop allows you to download data by country. Bul - Bulgaria, Swit - Switzerland, Zimb - Zimbabwe, Austria - Austri
state=Swit

# Specify the start and end year (including), for which you need data. For periods less than a month, use the case study script. For periods between a month and a year, you can download the whole year and only use the data you need:
start_year=1999
end_year=2020

# Specify output directories (they will be made if they don't exist):
ogimet_downloads_dir='../1_ogimet_raw_downloads_'$state # The directory where the raw ogimet synops will be downloaded and stored. You can specify a full path.
synop_tables_dir='../2_synop_tables_'$state # The directory where the decoded synops will appear. You can specify a full path.

# END OF USER SPECIFICATIONS
##############################################################################################################


# Remembers when the script started.
begin_time=$(date)

# Removing old <raw_data_*> files in the directory, if existing.
echo "Deleting old < raw_data_* > files, if any."
rm -rf raw_data_*

# Making user directories, if they don't exist.
if [ ! -d $ogimet_downloads_dir ]; then
  mkdir $ogimet_downloads_dir
fi

if [ ! -d $synop_tables_dir ]; then
  mkdir $synop_tables_dir
fi

# Removing old <list-ogimet> file, if existing.
if [ -f list_ogimet ]; then
  echo "Deleting old < list-ogimet > file."
  rm -rf list-ogimet
fi

# Exit if <wmo-index-bbox> is not created.
if [ ! -f $WMO_FILE ]; then
  echo "File < $WMO_FILE > doesn't exist."
  exit 1
fi

yearlist=$(seq $start_year $end_year)
months="01 02 03 04 05 06 07 08 09 10 11 12"

  
# Downloads data month by month.
for year in $yearlist ; do
  for month in $months ; do
    # Defining period strings.
    lc=$(expr $year % 4)
    # lc means leap check 
    if [ $month -eq "01" ] || [ $month -eq "03" ] || [ $month -eq "05" ] || [ $month -eq "07" ] || [ $month -eq "08" ] || [ $month -eq "10" ] || [ $month -eq "12" ]; then
      num_days=31
    elif [ $month -eq "04" ] || [ $month -eq "06" ] || [ $month -eq "09" ] || [ $month -eq "11" ]; then
      num_days=30
    elif [ $month -eq "02" ] && [ $lc -eq 0 ]; then
      num_days=29
    elif [ $month -eq "02" ] && [ ! $lc -eq 0 ]; then
      num_days=28
    fi

    start_year=$year
    start_month=$month
    start_day=01
    end_year=$year
    end_month=$month
    end_day=$num_days
    start_hour_minute=0000
    end_hour_minute=2300
    period_start="$start_year$start_month$start_day$start_hour_minute"
    period_end="$end_year$end_month$end_day$end_hour_minute"
    echo "Period set from $period_start to $period_end"
    
    # Infinite loop until ogimet responds positively.
    while [ 1 -eq 1 ]; do
      echo Downloading for state: $state
      wget http://www.ogimet.com/cgi-bin/getsynop?begin=$period_start\&end=$period_end\&state=$state\&lang=eng -O out
      string=$(cat out)
      if echo "$string" | grep 'Status'; then
        echo "The server didn't let you download the file. Going to try again in 10 minutes."
        sleep 10m
      else
        cat out >> raw_data_$state\_$year
        break
        fi
      done
    echo "Sleeping for 3 minutes before next download attempt"
    sleep 3m
    done # for months

  # The file <list_ogimet> is needed by <decode_synop.x>. In this case it contains only the path to one file. 
  echo raw_data_$state\_$year >> list_ogimet
  done # for years
echo "Completed download process."

# For every line in <wmo-index-file>, take the 5 five digits as WMO index, dowlnoad the station data and decode it.
while read line; do
  echo $line
  WMO=$(echo $line | cut -c1-5)
  echo line $line
  echo wmo $WMO
  echo line $line
  ./decode_synop.x $WMO

  # Insert station meta data in the beginning of the file and delete empty files.
  for dat_file in $(ls data_$WMO\_*); do 
    # if [ -f $dat_file ]; then
      sed -i "1s%^%Station Meta Data: $line\n%" $dat_file
      n_lines=$(wc -l < $dat_file)
      if [ $n_lines -le 2 ]; then  # <=
        rm -rf $dat_file    
      else
        mv $dat_file $synop_tables_dir
      fi
    # fi
  done 

  echo "Completed decode process for wmo: $WMO"
done < $WMO_FILE

cp $WMO_FILE $ogimet_downloads_dir 
mv raw_data_* $ogimet_downloads_dir
mv list_ogimet $ogimet_downloads_dir

# Prints the start and end time of the script.
end_time=$(date)
echo "Your script started: $begin_time , and ended: $end_time ."
echo "Process completed succesfully."
exit 0

