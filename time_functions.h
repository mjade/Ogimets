//
// Created by boriana on 1/29/18.
//

#ifndef SYNOP_TIME_FUNCTIONS_H
#define SYNOP_TIME_FUNCTIONS_H

#include <iostream>
#include "math.h"
#include "string.h"
#include <stdlib.h>
#include <stdio.h>      /* puts, printf */
#include <time.h>       /* time_t, struct tm, time, gmtime */


int make_timestamp (std::string date_from_synop) {

    std::string y1=date_from_synop.substr(6, 4);
    int year=atoi(y1.c_str());
    //cout<<"the year"<<year<<endl;
    std::string m1=date_from_synop.substr(11, 2);
    int month=atoi(m1.c_str());
    //cout<<"month"<<month<<endl;
    std::string d1=date_from_synop.substr(14, 2);
    int day=atoi(d1.c_str());
    //cout<<"day"<<day<<endl;
    std::string h1=date_from_synop.substr(17, 2);
    int hour=atoi(h1.c_str());
    //cout<<"hour"<<hour<<endl;
    std::string min1=date_from_synop.substr(20, 2);
    int min=atoi(min1.c_str());

    //cout<<"min"<<min<<endl;
    int sec=00;

    //cout<<"conversion to ints done"<<endl;

    struct tm t;
    time_t t_of_day;
    t.tm_year = year-1900;
    t.tm_mon = month-1;           // Month, 0 - jan
    t.tm_mday = day;          // Day of the month
    t.tm_hour = hour;
    t.tm_min = min;
    t.tm_sec = sec;
    t.tm_isdst = 0;        // Is DST on? 1 = yes, 0 = no, -1 = unknown
    t_of_day = timegm(&t);

    //    printf("seconds since the Epoch: %ld\n", (long) t_of_day);

    /* call mktime: create unix time stamp from timeinfo struct */
    return t_of_day;
}

std::string get_year (std::string date_from_synop) {

    std::string y1=date_from_synop.substr(6, 4);
    return y1;
}



std::string get_date (std::string date_from_synop) {
    std::string y1=date_from_synop.substr(6, 4);
    std::string m1=date_from_synop.substr(11, 2);
    std::string d1=date_from_synop.substr(14, 2);

    std::string dddd=y1+"-"+m1+"-"+d1;
    return dddd;
}

std::string get_date_n_hour (std::string date_from_synop) {
    std::string y1=date_from_synop.substr(6, 4);
    std::string m1=date_from_synop.substr(11, 2);
    std::string d1=date_from_synop.substr(14, 2);

    std::string hh=date_from_synop.substr(17, 2);
    std::string mm=date_from_synop.substr(20, 2);

    std::string ddddhh=y1+"-"+m1+"-"+d1+"-"+hh+":"+mm;
    return ddddhh;
}


double get_year_fraction (std::string date_from_synop) {
// DOESN'T WORK !!!!! DON'T USE!!!!

    int alldays, daysm_sum;
    double frac;
    char buffer[10];

    std::string y1 = date_from_synop.substr(6, 4);
    int year = atoi(y1.c_str());
    std::string m1 = date_from_synop.substr(11, 2);
    int month = atof(m1.c_str());
    std::string d1 = date_from_synop.substr(14, 2);
    int day = atof(d1.c_str());
    std::string h1 = date_from_synop.substr(17, 2);
    int hour = atof(h1.c_str());

    struct tm t;
    time_t t_of_day;
    t.tm_year = year - 1900;
    t.tm_mon = month - 1;           // Month, 0 - jan
    t.tm_mday = day;          // Day of the month
    t.tm_hour = hour;

    if (year % 4 == 0)
        alldays = 366;
    else
        alldays = 365;

    strftime(buffer, 10, "%j", &t);
    puts(buffer);
    daysm_sum = atoi(buffer) - day;
    printf("%i\n", daysm_sum);
    printf("%f %f %f %f %f\n", float(year), float(daysm_sum), float(month), float(hour), float(alldays));

    frac = float(year) + (float(daysm_sum) * 24. + (float(month) - 1.) * 24. + float(hour)) / (float(alldays) * 24.);
    printf("%4.8f \n", frac);
    return frac;
}

#endif //SYNOP_TIME_FUNCTIONS_H