#include <iostream>
#include <fstream>
#include "string.h"
#include <stdlib.h>
#include <stdio.h>      /* puts, printf */
#include <sstream>
#include <string>
using namespace std;

int count = 0;
std::string newfile;
std::string testline, test_N, test_E;
std::string test_LAT, test_LON;
double LAT, LON;
float pm_N, pm_E;
float bbox_N, bbox_S, bbox_W, bbox_E;

// test for antarctica - d01
//float bbox_N = -57.98066;
//float bbox_S = -66.64827;
//float bbox_W = -71.16992;
//float bbox_E = -51.830902;

// test for antarctica - d03
//float bbox_N = -62.12486;
//float bbox_S = -63.03471;
//float bbox_W = -61.55951;
//float bbox_E = -59.086365;

int main(int argc, char *argv[]) {
    if (argc != 5) {
        cout<<"Too many or too few arguments"<<endl;
        exit(1);
    }

    bbox_N = atof(argv[1]);
    bbox_S = atof(argv[2]);
    bbox_W = atof(argv[3]);
    bbox_E = atof(argv[4]);

    if ((bbox_N <= bbox_S) || (bbox_W >= bbox_E)) {
        cout << "Error in bounding box ranges. <N> should be larger than <S> and <E> should be larger than <W>."<< endl;
        exit(1);
    }

    fstream AllWMO;
    AllWMO.open("wmo-index-all");
    if (!AllWMO) {
        cout<<"File <wmo-index-all> not in source directory"<<endl;
        exit(1);
    }

    ofstream BboxWMO;
    BboxWMO.open("wmo-index-bbox");

    while (!AllWMO.eof()) {

        getline(AllWMO, testline);
        if (testline.length() < 49)
            continue;
        test_N = testline.substr(41, 1);
        test_E = testline.substr(48, 1);
        if (strcmp(test_N.c_str(), "N") == 0)
            pm_N = 1.;
        else if (strcmp(test_N.c_str(), "S") == 0)
            pm_N = -1.;
        if (strcmp(test_E.c_str(), "W") == 0)
            pm_E = -1.;
        else if (strcmp(test_E.c_str(), "E") == 0)
            pm_E = 1.;
        test_LAT = testline.substr(37, 4);
        test_LON = testline.substr(43, 5);
        LAT = atoi(test_LAT.c_str()) / 100. * pm_N;
        LON = atoi(test_LON.c_str()) / 100. * pm_E;
        //printf("%s \n", testline.c_str());
        //printf("%i, %f, %f \n", count, LAT, LON);

        if ((LAT < bbox_N) && (LAT > bbox_S)) {
            if ((LON > bbox_W) && (LON < bbox_E)) {
                count += 1;
                printf("%i, %f, %f \n", count, LAT, LON);
                BboxWMO << testline << endl;
            }
        }
    }
    return 0;
}
