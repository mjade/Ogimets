This set of Bash scripts and C++ codes is intended to automatically select, 
download and decode observational SYNOP data from *ogimet.com* for a user-defined
set of coordinates and time period. The set of codes consists of two main
scripts: **Ogimets_case_study.sh** and  **Ogimets_climate.sh**.
The *case srudy* script is intended for data download for user-defined periods of 
less than a month, while the *climate* script downloads data for a set of years.


The *selecting routine* asks the user to define a rectangle bounding box with a
set of coordinates. A list of all WMO stations with their coordinates and
elevation is taken from [1] and stored in the file **wmo-index-all**. The
*downloading routine* is accomplished via wget and the getsynop cgi binary, 
provided by ogimet. The *decoding routine* of the SYNOP code follows the
instructions from [2]. The decoding routine is currently implemented only for 
the variables: temperature, humidity, dew point temperature, wind direction, 
wind speed (in accordance with the wind type indicator), station level pressure,
sea level pressure and sea surface temperature (from group 222).


The output data is stored in two user-specified directories - one for the raw 
SYNOP data - DIR1 (as downloaded from ogimet.com, because decoding is without a
warranty) and another directory for the decoded data - DIR2, ready for plotting.
Furthermore, a list of all WMO stations in your rectangle is stored in the file
**wmo-index-bbox** in DIR1. The decoded data are stored in separate files for 
every variable and every station. For example, the temperature data for Varna
will be stored in a file named data_15552_temp.dat, where 15552 is the WMO 
index of Varna. The values in 
the .dat files are written in two columns: time of observation and value, where 
observational times without a value are omitted. If there are no values for the 
file variable for the selected period, a file with no records will still be 
created. For convenience, the first line of every .dat file
contains the name, coordinate and elevation of the station (the line string from
[1]); the second line indicates the variable and measurement unit; records start 
from the third line. Note that there may be small chronological displacements in
the data, as there is no rearranging routine. Also note that no quality control
checks are done upon the data - it comes as it is.


You can find the download and usage instructions for both scrips below.


*  **Downloading the source code and compilation:**
 1. Make a working directory, where you would probably want to store your data.
 
 `mkdir ogimet`

 2. Make a directory for the source code inside it.

 `cd ogimet/`
 
 `mkdir src`
 
 3. Clone the repository.
  
 `cd src/`
  
 `git clone https://gitlab.com/mjade/Ogimets`
 
 4. (Optional) Compile the .cpp files into binaries. The compiled binaries are
 already provided and don't need to be recompiled on the current machine.
 
 `g++ -o find_in_bbox.x find_in_bbox.cpp`

 `g++ -std=c++0x -o decode_synop.x decode_synop.cpp`
 
 5. Make the script files executable.
 
 `chmod +x *.sh`



*  **Getting data for a case study**

For time periods of a month or less one can use **Ogimets_case_study.sh**.
Open the script with a text editor of your choice and specify your subset in the
USER SCPECIFICATIONS field. The subsets in the source code are for the area 
around Livingstone Island from February 1 to February 5, 2017. The raw ogimet 
output directory (DIR1) in the source script is  set to 
*../1_ogimet_raw_downloads* and the decoded data 
directory (DIR2) is set to *../2_synop_tables*. They will be created, if they 
don't exist. It is recommended that you use different directories for different
data subsets. 



*  **Getting data for climate studies**

For climate studies of longer time periods, one can use the other script  
**Ogimets_climate.sh**. The only difference is that instead of time period, one 
should only define start and end year (including). The years may be equal to 
each other if you need data for only one year. The script automatically defines
the periods in the getsynop cgi binary (Technical note: the periods are defined
month by month, because of the ogimet query limit). The output directories and 
files are the same as in the previous script.

Instead of specifing a bounding box, in the **Ogimets_climate.sh** the user
specifies a country. This way getsynop is able to download data from many SYNOP 
stations at once instead of querying each WMO index individually.

Don't run multiple scripts at the same time from the same IP, because you will
exceed you ogimet query limit.

As it is easy to exceed your limit for queries in ogimet, all scripts are with
a time delay between downloads. This is the reason why they take so long to
exexcute. It is recommended that the user runs them in the background overnight
with standard output and standard error redirected to a log file:

`nohup ./Ogimets_case_study.sh &> log &` or `nohup ./Ogimets_climate.sh &> log &`



The .dat files can be easily visualised in **gnuplot** with:

`set xdata time; set timefmt "%Y-%m-%d-%H-%M"; set format x "%Y %b"; set xtics rotate`

`plot "data_15552_temp.dat" u 1:2 w points` 



Thank you for using the **Ogimets** set of Bash scripts and C++ codes. For any 
questions or discussions, you can contact me at bshtirkova -at- uni-sofia.bg


References:

[1] Coordinates of WMO stations: http://old.wetterzentrale.de/klima/stnlst.html

[2] SYNOP decoding: https://rda.ucar.edu/datasets/ds463.0/docs/codes.pdf

