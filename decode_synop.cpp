#include <iostream>
#include <fstream>
#include "math.h"
#include "string.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sstream>
#include <string>
#include"time_functions.h"
using namespace std;

// You have to compile with C++11 support:
// g++ -std=c++0x  -o decode_synop.x decode_synop.cpp

// The program takes only one argument - the 5 digit WMO index.

// Reads the temperature, humidity, dew point temperature, wind direction and wind speed (in accordance to the
// wind type indicator), station pressure and sea level pressure from SYNOP records, downloaded from ogimet (one record
// per line). Also decodes sea surface temperature in the group 222, as sent from Bulgarian and Romanian stations.
// Referral link for SYNOP decoding: https://rda.ucar.edu/datasets/ds463.0/docs/codes.pdf .

//WMO-index of Varna 15552
//WMO-index of Burgas 15655
//WMO-index of Constanta 15480


std::string wmo, wmo_s;
std::string name_filelist = "list_ogimet";
const int imax=50000, jmax=90;
std::string code[imax][jmax];
std::string newfile, outfile, yyyy, test, test2, testline, testline2, times_s[imax], eli_s, sign_s, hope;
std::string wt_ind_s, wdir_s, wspd_s, clouds_s;
int test_int, test2_int;
int i=0, ic=0, j=0, count=0, eli, sign, pm;
int wt_ind;
float temp_f, sst_f;
int temp[imax], hum[imax], dp_temp[imax], clouds[imax], wdir[imax], sst[imax];
float wspd[imax], slp[imax], stpres[imax];

int main(int argc, char** argv) {

    if (argc == 2) {
        wmo = argv[1];
        cout<<"Starting to decode synops with wmo: "<<wmo<<endl;
    } else if (argc == 1) {
        cout<<"Please specify a 5 digit wmo index and make sure you have put the path to synop files in: \n"
            <<name_filelist<<endl;
        exit(1);
    } else {
        cout<<"Too many arguments."<<endl;
        exit(1);
    }

    wmo_s=wmo;
    std::string outfile_temp="data_"+wmo_s+"_temp.dat";
    std::string outfile_hum="data_"+wmo_s+"_hum.dat";
    std::string outfile_dp="data_"+wmo_s+"_dp.dat";
    std::string outfile_clouds="data_"+wmo_s+"_clouds.dat";
    std::string outfile_wdir="data_"+wmo_s+"_wdir.dat";
    std::string outfile_wspd="data_"+wmo_s+"_wspd.dat";
    std::string outfile_stpres="data_"+wmo_s+"_stpres.dat";
    std::string outfile_slp="data_"+wmo_s+"_slp.dat";
    std::string outfile_sst="data_"+wmo_s+"_sst.dat";
    ofstream Table_temp;
    ofstream Table_hum;
    ofstream Table_dp;
    ofstream Table_clouds;
    ofstream Table_wdir;
    ofstream Table_wspd;
    ofstream Table_stpres;
    ofstream Table_slp;
    ofstream Table_sst;
    Table_temp.open(outfile_temp);
    Table_hum.open(outfile_hum);
    Table_dp.open(outfile_dp);
    Table_clouds.open(outfile_clouds);
    Table_wdir.open(outfile_wdir);
    Table_wspd.open(outfile_wspd);
    Table_stpres.open(outfile_stpres);
    Table_slp.open(outfile_slp);
    Table_sst.open(outfile_sst);
    Table_temp << "Date-Time \t Temperature [degC]"<<endl;
    Table_hum << "Date-Time \t Humidity [%]"<<endl;
    Table_dp << "Date-Time \t DewPointTemperature [degC]"<<endl;
    Table_clouds << "Date-Time \t CloudOctas [0-9]"<<endl;
    Table_wdir << "Date-Time \t WindDirection [deg]"<<endl;
    Table_wspd << "Date-Time \t WindSpeed [m/s]"<<endl;
    Table_stpres << "Date-Time \t Pressure at station [hPa]"<<endl;
    Table_slp << "Date-Time \t Sea level pressure [hPa]"<<endl;
    Table_sst << "Date-Time \t Sea SurfaceTemperature [degC]"<<endl;

    // cout<<"So, it begins..."<<endl;

    // ofstream Table_errors;  // This is used because you can often read sst = 0 degC from the Varna report
    // Table_errors.open("errors.txt");
    fstream filelist;
    filelist.open(name_filelist);
    fstream synop_file;
    while(!filelist.eof()) {
        //Puts nines in the array
        // cout<<"got next line-outside loop"<<endl;

        getline(filelist, newfile);
        synop_file.open(newfile.c_str());
        // cout<<"file opened:  "<<newfile<<endl;
        if (!synop_file) {
            cout << "Reached the end of file list. Done!"<<endl;
            cout << "Values written to dat files"<<endl;
            exit(0);   // call system to stop
        }

        //  cout<<"Initializing arrays, they're big arrays ..."<<endl;
        for (i=0; i<imax; i++) {
                temp[i] = 999;
                hum[i] = 999;
                dp_temp[i] = 999;
                wdir[i] = 999;
                wspd[i] = 999.0;
                stpres[i] = 9999.0;
                slp[i] = 9999.0;
                sst[i] = 999;
        }
        // cout<<"done"<<endl;

        i=0, j=0, count=0;

        while(!synop_file.eof())
        {
            // cout<<"got next line"<<endl;
            count=count+1;
            // cout<<count<<endl;
            getline(synop_file, testline);
            // cout<<"gotline, testline l"<<testline.length()<<endl;

            if (testline.length() < 27) {
                // cout<<"inside if"<<endl;
                continue;
            }
            // cout<<"after if"<<endl;

            //So that it takes only synops for the specified wmo-index
            test=testline.substr(0,5);
            /* test_int=atoi(test.c_str()); */
            if (test == wmo_s)
            {

                times_s[i]=testline.substr(0,27);
                testline2=testline.erase(0,27);
                // cout<<"times"<<times_s<<endl;
                stringstream ss(testline2);
                while (ss)
                {
                    ss>>code[i][ic];
                    ic++;
                }
                ic=0;
                i++;
            }
        }

        synop_file.close();
        int array_size=i;
        // cout << "Array size is: " << array_size << endl;

        if (array_size == 0) {
            cout<<"Empty file, or no such WMOs in this file"<<endl;
            continue;
        }

        for(i=0; i<=array_size; i++) {    //This loops on the rows.
            // cout << "i is = " << i << " j is = " << j << endl;
            if (code[i][0].empty()) {
                array_size = i-1;
                // cout<<"empty array"<<endl;
                break;
            }
            if (code[i][0].length() < 5){
                // cout<<"short"<<endl;
                continue;
            }

            wt_ind_s = code[i][0].substr(4,1);
            wt_ind = atoi(wt_ind_s.c_str());
            // wind type indicator
            // 0, 1 - m/s; 2, 3 - knots

            test = code[i][1];
            test_int = atoi(test.c_str());
            if (test == wmo_s){
                if (code[i][3].length() < 5) {
                    // cout<<"short 2"<<endl;
                    continue;
                }
                clouds_s = code[i][3].substr(0,1);
                wdir_s = code[i][3].substr(1,2);
                wspd_s = code[i][3].substr(3,2);
                clouds[i] = atoi(clouds_s.c_str());
                if (wt_ind >= 2)
                    wspd[i]=atof(wspd_s.c_str())/2.0;
                else if (wt_ind <=1)
                    wspd[i]=atof(wspd_s.c_str());
                wdir[i]=atoi(wdir_s.c_str())*10;
                //wdir in degrees and wspd in m/s


                for(j=4; j<=7; j++) { // This loops on the columns
                    // cout<<j<<" "<<code[i][j].size()<<endl;
                    if (code[i][j].length() < 5 )
                        break;

                    // check for Temperature
                    test = code[i][j].substr(0,1);
                    test_int = atoi(test.c_str());

                    if (test_int == 1) {
                        eli_s = code[i][j].substr(2, 3);
                        sign_s = code[i][j].substr(1, 1);
                        eli = atoi(eli_s.c_str());
                        sign = atoi(sign_s.c_str());

                        if (sign == 1) {
                            pm = -1;
                            temp[i] = eli * pm;
                        } else if (sign == 0) {
                            pm = 1;
                            temp[i] = eli * pm;
                        }
                        // it's in decimals because of integer array, divided by 10 later in the code
                    }

                    if (test_int == 2) {
                        eli_s = code[i][j].substr(2, 3);
                        sign_s = code[i][j].substr(1, 1);
                        eli = atoi(eli_s.c_str());
                        sign = atoi(sign_s.c_str());

                        if (sign == 1) {
                            pm = -1;
                            dp_temp[i] = eli * pm;
                        } else if (sign == 0) {
                            pm = 1;
                            dp_temp[i] = eli * pm;
                        } else if (sign == 9) {
                            hum[i] = eli;
                        }

                    }

                    if (test_int == 3) {
                        eli_s = code[i][j].substr(2, 3);
                        sign_s = code[i][j].substr(1, 1);
                        eli = atoi(eli_s.c_str());
                        sign = atoi(sign_s.c_str());

                        if (sign == 0)
                            stpres[i] = 1000.0 + eli / 10.0;
                        else if (sign <= 9)
                            stpres[i] = sign*100.0 + eli / 10.0;
                    }

                    if (test_int == 4) {
                        eli_s = code[i][j].substr(2, 3);
                        sign_s = code[i][j].substr(1, 1);
                        eli = atoi(eli_s.c_str());
                        sign = atoi(sign_s.c_str());

                        if (sign == 0)
                            slp[i] = 1000.0 + eli / 10.0;
                        else if (sign <= 9)
                            slp[i] = sign*100.0 + eli / 10.0;
                    }

                }

                for (j=4; j<=jmax; j++) {
                    if (code[i][j].length() < 3 )
                        continue;
                    test2 = code[i][j];

                    if (test2 == "222" || test2 == "222//") {  // 222 preceeds the ocean data record in the synop report
                        // cout << "found the group, it's at count i, j:" << i << j << "  time  " << times_s[i] << endl;
                        for (int a = 1; a < 10; a++) {
                            if (code[i][j + a].length() >= 3) { test = code[i][j + a].substr(0, 3); }
                            else { continue; }
                            if (test == "333" || test == "555")
                                break;
                            if (code[i][j + a].length() >= 5) { test2 = code[i][j + a].substr(0, 5); }
                            else { continue; }
                            if (test2 == "0////" || test2 == "00//0" || test2 == "00///")
                                break;

                            hope = code[i][j + a].substr(0, 1);
                            if (hope == "0") {

                                eli_s = code[i][j + a].substr(2, 3);
                                sign_s = code[i][j + a].substr(1, 1);
                                eli = atoi(eli_s.c_str());
                                sign = atoi(sign_s.c_str());

                                if (sign == 1)
                                    pm = -1;
                                else if (sign == 0)
                                    pm = 1;
                                sst[i] = eli *
                                         pm;  // it's in decimals because of integer array, divided by 10 later in the code
                            } else { continue; }
                        }
                    }
                }
            }
        }

        std:string x, y;
        double y_double;

        // Takes the data from the arrays and writes it to separate files, omitting nan (999) values.
        for (int i=0; i<=array_size; i++) {
            //if (sst[i] == 0) {
            // Table_errors<<"we have 0degC at "<<times_s[i]<<endl;
            // }
            x = get_date_n_hour(times_s[i]);

            if (temp[i] != 999) {
                temp_f=temp[i]/10.0;
                Table_temp<<x<<"\t"<<temp_f<<endl;
            }

            if (hum[i] != 999) {
                Table_hum<<x<<"\t"<<hum[i]/10.0<<endl;
            }

            if (dp_temp[i] != 999) {
                temp_f=dp_temp[i]/10.0;
                Table_dp<<x<<"\t"<<temp_f<<endl;
            }

            if (clouds[i] != 999) {
                Table_clouds<<x<<"\t"<<clouds[i]<<endl;
            }

            if (wdir[i] != 999) {
                Table_wdir<<x<<"\t"<<wdir[i]<<endl;
            }

            if (wspd[i] != 999.0) {
                Table_wspd<<x<<"\t"<<wspd[i]<<endl;
            }

            if (stpres[i] != 9999.0) {
                Table_stpres<<x<<"\t"<<stpres[i]<<endl;
            }

            if (slp[i] != 9999.0) {
                Table_slp<<x<<"\t"<<slp[i]<<endl;
            }

            if (sst[i] != 999 && sst[i] != 0) {
                sst_f=sst[i]/10.0;  // divided by 10, because the previous value was in decimals
                Table_sst<<x<<"\t"<<sst_f<<endl;
            }
        }
    }

    Table_temp.close();
    Table_hum.close();
    Table_dp.close();
    Table_clouds.close();
    Table_wdir.close();
    Table_wspd.close();
    Table_stpres.close();
    Table_slp.close();
    Table_sst.close();
    cout<<"tables closed, all done"<<endl;
    //Table_errors.close();

    return 0;
}
