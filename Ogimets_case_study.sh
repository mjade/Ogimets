#!/bin/bash

# Case Study script - for periods less than a month. Please fill the user specifications section with corner coordinates, period and output directories.
# This script finds all WMO stations which report weather observations in SYNOP code in a user selected rectangle bounding box and period, downloads the data and decodes the data from the available values for temperature, humidity, dew point temperature, wind speed, wind direction, station pressure, sea level pressure and sea surface temperature. Sea surface temperature data is taken in synop group 222, because that's how they transmit it from Varna and Bourgas.
# The script takes a lot of time, because of the ogimet query. It's recommended to run it in the background with the output written in log files (and leave it for a night).

##############################################################################################################
# USER SPECIFICATIONS

# Specify the decimal coordinates (with negative values for the coordinates South of the Equator and West of Greenwich):
LAT_max=-62.12486  # the northernmost point of the rectangle
LAT_min=-63.03471  # the southernmost point of the rectangle
LON_min=-61.55951  # the westernmost point of the rectangle
LON_max=-59.086365  # the easternmost point of the rectangle

# Specify the period (should be less than a month, otherwise use the climate script and specify only years in the yearlist.txt file):
start_year=2017  # four digit start year
start_month=02   # two digit start month
start_day=01     # two digit start day

end_year=2017    # four digit end year
end_month=02     # two digit end month
end_day=05       # two digit endday

# Specify output directories (they will be made if they don't exist):
ogimet_downloads_dir='../1_ogimet_raw_downloads' # The directory where the raw ogimet synops will be downloaded and stored. You can specify a full path.
synop_tables_dir='../2_synop_tables' # The directory where the decoded synops will appear. You can specify a full path.

# END OF USER SPECIFICATIONS
##############################################################################################################

# Remembers when the script started.
begin_time=$(date)

# Removing old <raw_data_*> files in the directory, if existing.
echo "Deleting old < raw_data_* > files, if any."
rm -rf raw_data_*

# Making user directories, if they don't exist.
if [ ! -d $ogimet_downloads_dir ]; then
  mkdir $ogimet_downloads_dir
fi

if [ ! -d $synop_tables_dir ]; then
  mkdir $synop_tables_dir
fi

# Removing old <wmo-index-bbox> file, if existing.
WMO_FILE=wmo-index-bbox
if [ -f $WMO_FILE ]; then
  echo "Deleting old < $WMO_FILE > ."
  rm -rf wmo-index-bbox
fi

# Defining period strings.
hour_minute=0000
period_start="$start_year$start_month$start_day$hour_minute"
period_end="$end_year$end_month$end_day$hour_minute"
echo "Period set from $period_start to $period_end"

# Find the wmo stations in the bounding box. Produces the file <wmo-index-bbox>.
./find_in_bbox.x $LAT_max $LAT_min $LON_min $LON_max

# Exit if <wmo-index-bbox> is not created.
if [ ! -f $WMO_FILE ]; then
  echo "File < $WMO_FILE > doesn't exist. Error in < find_in_bbox.x >."
  exit 1
fi

# For every line in <wmo-index-file>, take the 5 five digits as WMO index, dowlnoad the station data and decode it.
while read line; do
  echo $line
  WMO=$(echo $line | cut -c1-5)
  echo line $line
  echo wmo $WMO
  echo line $line
  
  # Infinite loop until ogimet responds positively.
  while [ 1 -eq 1 ]; do
    echo Downloading for wmo: $WMO
    wget http://www.ogimet.com/cgi-bin/getsynop?block=$WMO\&begin=$period_start\&end=$period_end -O out
    string=$(cat out)
    if echo "$string" | grep 'Status'; then
      echo "The server didn't let you download the file. Going to try again in 10 minutes."
      sleep 10m
    else
      cat out >> raw_data_$WMO
      # Decode synop and break loop
      # The file <list_ogimet> is needed by <decode_synop.x>. In this case it contains only the path to one file. 
      echo raw_data_$WMO > list_ogimet
      ./decode_synop.x $WMO

      # Insert station meta data in the beginning of the file and delete empty files.
      for dat_file in $(ls data_$WMO\_*); do
        sed -i "1s%^%Station Meta Data: $line\n%" $dat_file
        n_lines=$(wc -l < $dat_file)
        if [ $n_lines -eq 2 ]; then
          rm -rf $dat_file
        else
          mv $dat_file $synop_tables_dir  
          fi
        done

      mv raw_data_$WMO $ogimet_downloads_dir
      echo "Completed download and decode process for wmo: $WMO"
      break
    fi
    done
  echo "Sleeping for 3 minutes before next download attempt"
  sleep 3m
done < "$WMO_FILE"

cp $WMO_FILE $ogimet_downloads_dir

# Prints the start and end time of the script.
end_time=$(date)
echo "Your script started: $begin_time , and ended: $end_time ."
echo "Process completed succesfully."
exit 0
